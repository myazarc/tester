/**
 * Executes a shell command and return it as a Promise.
 * @param cmd {string}
 * @return {Promise<string>}
 */
function execShellCommand(cmd) {
  const exec = require("child_process").exec;
  return new Promise((resolve, reject) => {
    exec(cmd, (error, stdout, stderr) => {
      if (error) {
        console.warn(error);
      }
      resolve(stdout ? stdout : stderr);
    });
  });
}

class GitHelper {
  constructor(path) {
    this.path = path;
  }

  getLastCommit() {
    const command = `cd ${this.path} && git log --format="%H" -n 1`;
    return execShellCommand(command);
  }

  getFindChangeFiles(firstCommit, lastCommit) {
    const command = `cd ${
      this.path
    } && git diff-tree --no-commit-id --name-only -r ${firstCommit} ${lastCommit}`;
    return execShellCommand(command);
  }
}

module.exports = GitHelper;
