const GitHelper = require("./gitHelper");
const path = require("path");

const currPath = path.join(__dirname);

const gitHelper = new GitHelper(currPath);

gitHelper.getLastCommit().then(res => {
  const last = res.trim();
  gitHelper
    .getFindChangeFiles("6c193f745926b123fd942fd57013418f3fc2b9dd", last)
    .then(files => {
      return files.trim().split("\n");
    })
    .then(res => {
      console.log(res);
    });
});

//
/*
! 6c193f745926b123fd942fd57013418f3fc2b9dd

? 2.commit
! 2d195e4428c85e8c16b6b497179bf0f5e703fccc

! 66f0e0dc41009357fcf6b0187663914e2b21640e


? last commit 
! 79ad37c0ce1b48542fd1e0bdf93213fed3d9dcf8
*/
